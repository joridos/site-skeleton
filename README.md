# README #

This is a basic scratch for new sites based on the front end framework [Foundation 5](http://foundation.zurb.com).

## Features os the scratch page ##

* Ajax call methods
* Modal ajax loading
* [Font Awesome](http://fortawesome.github.io/Font-Awesome/) SVG icons ready
* Form validation with [bValidator](http://bojanmauser.from.hr/bvalidator/)